<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\FieldRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Hash;
use Crypt;
use Session;
use Validator;

class UserLoginController extends Controller
{
    public function __construct(){
    	$this->middleware('guest:web')->except('logout');
    }

    /**
    * @method: register
    * @params: request
    * @createddate: 25-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To register user and cook
    * @return:
    */
    public function register(FieldRequest $request){
        $role = Crypt::decrypt($request->role);
		$user = User::create([
			'name'=>$request['name'],
			'email'=>$request['email'],
			'role_id'=> $role,
			'password'=>Hash::make($request['password']),
		]);
		Auth::login($user);
        return $user;
    }

    /**
    * @method: register
    * @params: request
    * @createddate: 25-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To register user and cook
    * @return:
    */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return  response()->json(['errors'=>$Validator->errors()->all()]);     
        }
        
        if(Auth::guard('web')->attempt ([
                'email'=> $request->get('email'),
                'password' => $request->get('password'),
        ])) {

                $role_id = Auth::user()->role_id;
                return $role_id;
            }
            else {
                return Response()->json("please check your details",403);
            }
    }

    /**
    * @method: logout
    * @params: 
    * @createddate: 25-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To register user and cook
    * @return:
    */
    public function logout(){
        Auth::guard('web')->logout();
        Session::flush();
        return redirect('/');
    }
}
