<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use App\Category;
use App\Subcategory;
use App\Recipe;
use App\RecipeImage;
use App\Comment;
use App\LikeRecipe;
use App\FavouriteRecipe;
use App\Helper;
use App\UserComment;
use Auth;
use PDF;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
    * @method: index
    * @params: request
    * @createddate: 23-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to get the list of all recipes and search 
    * @return: 
    */
	public function index(Request $request){
        try{
            if($request->isMethod('post')){
                $keyword = Input::get('search');
                if ($keyword) {
                    $search = Helper::searchRecipe($request->search);
                    return view('Frontend.search_result',compact('search'));
                }else{
                    return redirect('/');
                }
            }
            else{
    		  $recipes = Recipe::with(['pics','likedrecipes'])->where('status',1)->latest()->get();
              $user = Auth::id();
              return view('Frontend.dashboard',compact('recipes'));
            }
        }
        catch(Exception $e){
            'something went wrong';
        }
	}

    /**
    * @method: recipeList
    * @params: id
    * @createddate: 19-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to get the selected recipe 
    * @return: 
    */
    public function recipeList($id){
        $id = Helper::decryptId($id);
        $selectedRecipe = Recipe::with(['pics','likedrecipes'])->where('subcategory_id',$id)->get();
        return view('Frontend.view_selected_recipe',compact('selectedRecipe'));
    }

    /**
    * @method: RecipeDetail
    * @params: id
    * @createddate: 19-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to show the selected recipe detail
    * @return: 
    */
    public function RecipeDetail($id){
        $id = Helper::decryptId($id);
        $rec = Recipe::with('pics')->findOrFail($id);
        $comment = Comment::where([['comment_status',1],['recipe_id',$id]])->latest()->get();
        return view('Frontend.detail_page',compact('rec','comment'));
    }

    /**
    * @method: userComments
    * @params: request
    * @createddate: 19-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to add a user comment
    * @return: 
    */
    public function userComments(Request $request){
        $recipe_id = Helper::decryptId($request->recipe_id);
        $validator = Validator::make($request->all(),[
                'comment_name'=> 'required|string|min:2|max:250',
                'rating'=>'required|numeric|min:1|max:5',
                'comment_pic'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $comment = new Comment;
        $comment->comment = $request->comment_name;
        $comment->recipe_id = $recipe_id;
        $comment->user_id = Auth::id();
        $comment->rating = $request->rating;
         if($request->hasFile('comment_pic')) {
            $comment_pic = $request->comment_pic;
            $filename = $comment_pic->getClientOriginalName();
            $comment_pic->move( public_path().'/frontend/images/', $filename);
            $comment->comment_pic = $filename;
        }
        
        $comment->save();
        return back()->with('success',"your comment has been submitted for review");
    }

      /**
    * @method: addFavouriteRecipeList
    * @params: $id
    * @createddate: 30-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to add a recipe to favourite list
    * @return: 
    */
    public function addFavouriteRecipeList($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $user = Auth::id();
        if($recipe){
            $favourite = FavouriteRecipe::where([['recipe_id',$id],['user_id',$user]])->first();
            if($favourite){
                return back()->with('danger','Recipe already added to your Favourite List');
            }
            else{
                $favourite = FavouriteRecipe::create([
                    'user_id'=>$user,
                    'recipe_id'=>$id
                ]);
                return back()->with('success','recipe added to favourite list');
            }
        }
        else {
            return back()->with('danger',"recipe not found");
        }        
    }

       /**
    * @method: removeFavouriteRecipeList
    * @params: $id
    * @createddate: 30-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to add a recipe to favourite list
    * @return: 
    */
    public function removeFavouriteRecipeList($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $user = Auth::id();
        if($recipe){
            $favourite = FavouriteRecipe::where([['recipe_id',$id],['user_id',$user]])->first();
            if($favourite){
                $favourite->delete();
                return back()->with('danger','recipe removed from favourite list');
            }
        }
        else {
            return "recipe not found";
        } 
    }

     /**
    * @method: likeRecipe
    * @params: $id
    * @createddate: 30-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to like recipe 
    * @return: 
    */
    public function likeRecipe($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $user = Auth::id();
        if($recipe){
            $like = LikeRecipe::where([['recipe_id',$id],['user_id',$user]])->first();
            if($like){
                $like->delete();
                return back()->with('danger','you disliked this recipe');
            }
            else{
                $likeStatus = LikeRecipe::create([
                    'user_id'=>$user,
                    'recipe_id'=>$id
                ]);
                return back()->with('success','you liked this recipe');
            }
        }           
        else {
            return "recipe not found";
        }
    }

     /**
    * @method: showFavouriteRecipeList
    * @params: 
    * @createddate: 30-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to show favourite recipe list for logged in user 
    * @return: 
    */
     public function showFavouriteRecipeList(){
        $user = Auth::id();
        $Favourite = FavouriteRecipe::where('user_id',$user)->pluck('recipe_id')->toArray();
        $userFavourite = Recipe::find($Favourite);
        return view('Frontend.favourite_list',compact('userFavourite'));
     }

     /**
    * @method: printRecipe
    * @params: $id
    * @createddate: 30-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to print recipe data
    * @return: 
    */
     public function printRecipe($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $pdf = PDF::loadView('Frontend.print_recipe',compact('recipe'));
        return $pdf->download('recipe.pdf');
     }

}


