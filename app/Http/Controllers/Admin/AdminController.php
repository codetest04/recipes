<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Recipe;
use App\RecipeImage;
use App\Category;
use App\Subcategory;
use App\Comment;
use App\UserComment;
use Validator;
use App\Helper;
use Auth;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
    * @method: index
    * @params: 
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To show the recipes list
    * @return:
    */
    public function index(){
        $rec_list = Recipe::with('pics')->latest()->get();
        return view('Admin.dashboard',compact('rec_list'));
    }

    /**
    * @method: selectCategory
    * @params: request data
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To show the sub-category according to selected category
    * @return:
    */
    public function selectCategory(Request $request){
        $category = Subcategory::where('category_id',$request->category)->get();
        return response()->json($category);
    }

    /**
    * @method: addRecipeForm
    * @params: 
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: show create a recipe form
    * @return:
    */
    public function addRecipeForm(){
        $category = Category::all();
        $subcategory = Subcategory::all();
        return view('Admin.add_recipes',compact('category','subcategory'));
    }

    /**
    * @method: addRecipe
    * @params: request data
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To create a recipe
    * @return:
    */
    public function addRecipe(Request $request){
        if($request->isMethod('post')) {
            $validator = Validator::make($request->all(),[
                'category'=>'required',
                'subcategory'=> 'required',
                'name'=> 'required|string|min:3|max:40|unique:recipes',
                'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'ingrediants'=>'required|string',
                'preparation'=>'required|string'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $recipe = new Recipe;
            $recipe->name = $request->name;
            $recipe->category_id = $request->category;
            $recipe->subcategory_id = $request->subcategory;
            $recipe->ingrediants = $request->ingrediants;
            $recipe->preparation = $request->preparation;
            $recipe->user_id = Auth::user()->id;
            $recipe->status = 1;
            $recipe->save();
            if($request->hasFile('recipe_pic')) {
                foreach($request->file('recipe_pic') as $image) {
                    $filename = $image->getClientOriginalName();
                    $image->move( public_path().'/frontend/images/', $filename);
                    $image = new RecipeImage;
                    $image->recipe_pic = $filename;
                    $recipe->pics()->save($image);
                }
            }
            return back()->with('success','Recipe added successfully');
        }
    }

    /**
    * @method: showRecipes
    * @params: id
    * @createddate: 19-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: view recipe detail
    * @return:
    */
    public function showRecipes($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        return view('Admin.view_recipes',compact('recipe'));
    }

    /**
    * @method: editRecipes
    * @params: id
    * @createddate: 19-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to display the edit recipe form 
    * @return:
    */
    public function editRecipes($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $category = Category::all();
        $selectedCategoryId = Recipe::where('id', $id)->pluck('category_id');
        $subcategory = Subcategory::where('category_id', $selectedCategoryId)->get();
        return view('Admin.edit_recipes',compact('recipe','category','subcategory'));
    }

    /**
    * @method: updateRecipe
    * @params: request data, id
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to update the recipe
    * @return:
    */
    public function updateRecipe(Request $request,$id){
        $id = Helper::decryptId($id);
        $validator = Validator::make($request->all(),[
            'category'=>'required',
            'subcategory'=>'required',
            'name'=> 'required|string|min:3|max:40|unique:recipes,name,'.$id,
            'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ingrediants'=>'required',
            'preparation'=>'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $recipe = Recipe::find($id);
        $recipe->name = $request->name;
        $recipe->category_id = $request->category;
        $recipe->subcategory_id = $request->subcategory;
        $recipe->ingrediants = $request->ingrediants;
        $recipe->preparation = $request->preparation;
        $recipe->save();
        if($request->hasFile('recipe_pic')) {
        $pic = RecipeImage::where('recipe_id', $id)->delete();
            foreach($request->file('recipe_pic') as $image) {
                $filename = $image->getClientOriginalName();
                $image->move( public_path().'/frontend/images/', $filename);
                $pic = new RecipeImage;
                $pic->recipe_id = $id;
                $pic->recipe_pic = $filename;
                $pic->save();              
            }
        }
        return back()->with('success','Recipe update successfully');
    }

    /**
    * @method: deleteRecipe
    * @params: id
    * @createddate: 22-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to delete a recipe
    * @return: 
    */
    public function deleteRecipe($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $recipe->delete();
        return back()->with('danger','Recipe deleted successfully');

    }

    /**
    * @method: getUserComments
    * @params: 
    * @createddate: 23-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to add a user comment
    * @return: 
    */
    public function getUserComments(){
        $getComment = Comment::all();
        return view('Admin.user_comment_list',compact('getComment'));
    }

     /**
    * @method: commentStatus
    * @params: 
    * @createddate: 23-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to active/inactive user comment
    * @return: 
    */
    public function commentStatus($id){
        $id = Helper::decryptId($id);
        $comment = Comment::findOrFail($id);
        $status = !empty($comment->comment_status) ? "0" : "1";
        $comment->comment_status = $status;
        $comment->update();
        // Comment::where('id', $id)->update(['status' => $status]);
        if($status==1){
            return back()->with('success','comment activate successfully');
        }
        else{
            return back()->with('danger','comment deactivated');
        }
    }

    /**
    * @method: viewComments
    * @params: $id
    * @createddate: 31-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to view user comment
    * @return: 
    */
    public function viewComments($id){
        $id = Helper::decryptId($id);
        $comment = Comment::findOrFail($id);
        return view('Admin.view_comments',compact('comment'));
    }

    /**
    * @method: viewComments
    * @params: $id
    * @createddate: 31-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to view user comment
    * @return: 
    */
    public function deleteComment($id){
        $id = Helper::decryptId($id);
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return back()->with('danger','comment deleted successfully');
    }

    /**
    * @method: recipeStatus
    * @params: recipe id 
    * @createddate: 26-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: to active/inactive user comment
    * @return: 
    */
    public function recipeStatus($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $status = !empty($recipe->status) ? "0" : "1";
        $recipe->status = $status;
        $recipe->update();
        if($status==1){
            return back()->with('success','recipe active successfully');
        }
        else{
            return back()->with('danger','recipe deactivated');
        }
    }
}
