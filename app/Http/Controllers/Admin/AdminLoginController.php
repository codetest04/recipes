<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Session;
use Validator;

class AdminLoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest:admin')->except('logout');
    }

    public function getLogin(){
        return view('layouts.admin_login');
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        if(Auth::guard('admin')->attempt ([
                'email'=> $request->get('email'),
                'password' => $request->get('password'),
                'role_id'=> 2
            ])) {
                return redirect('admin/index');
            }
            else {
                return back()->with('danger',"please check yout details");
            }
        }

    public function logout(){
        Auth::guard('admin')->logout();
        Session::flush();
        return redirect('admin/login');
    }
}
