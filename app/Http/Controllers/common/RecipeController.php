<?php

namespace App\Http\Controllers\common;

use Illuminate\Http\Request;
use Validator;
use App\Recipe;
use App\RecipeImage;
use App\Http\Controllers\Controller;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.add_recipe');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=> 'required|string|min:3|max:40|unique:recipes',
            'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ingrediants'=>'required',
            'preparation'=>'required'
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $recipe = new Recipe;
        $recipe->name = $request->name;
        $recipe->ingrediants = $request->ingrediants;
        $recipe->preparation = $request->preparation;
        $recipe->save();

        if($request->hasFile('recipe_pic')) {
            foreach($request->file('recipe_pic') as $image) {
                $filename = $image->getClientOriginalName();
                $image->move( public_path().'/frontend/images/', $filename);
                $image = new RecipeImage;
                $image->recipe_pic = $filename;
                $recipe->pics()->save($image);
            }
        }

        return back()->with('success','Recipe added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rec = Recipe::with('pics')->find($id);
        return view('Frontend.detail_page',compact('rec'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = Recipe::with('pics')->find($id);
        return view('Frontend.edit_recipe',compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'=> 'required|string|min:3|max:40|unique:recipes,name,'.$id,
            'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ingrediants'=>'required',
            'preparation'=>'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $recipe = Recipe::find($id);
        $recipe->name = $request->name;
        $recipe->ingrediants = $request->ingrediants;
        $recipe->preparation = $request->preparation;
        $recipe->save();
        // $pic = new RecipeImage;
        if($request->hasFile('recipe_pic')) {
        $pic = RecipeImage::where('recipe_id', $id)->delete();

            foreach($request->file('recipe_pic') as $image) {
                $filename = $image->getClientOriginalName();
                $image->move( public_path().'/frontend/images/', $filename);
                $pic = new RecipeImage;
                $pic->recipe_id = $id;
                $pic->recipe_pic = $filename;
                $pic->save();              
            }
        }
        return back()->with('success','Recipe update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
