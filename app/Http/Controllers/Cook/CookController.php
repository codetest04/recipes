<?php

namespace App\Http\Controllers\Cook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helper;
use App\Category;
use App\Subcategory;
use App\RecipeImage;
use App\Recipe;
use Auth;

class CookController extends Controller
{
    public function __construct(){
    	$this->middleware('auth:web');
    }

    public function index(){
        $id = Auth::id();
        $rec_list = Recipe::with('pics')->where('user_id',$id)->latest()->get();
    	return view('Cook.dashboard',compact('rec_list'));
    }

    /**
    * @method: addRecipeForm
    * @params: 
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: show create a recipe form
    * @return:
    */
    public function addRecipeForm(){
        $category = Category::all();
        $subcategory = Subcategory::all();
        return view('Cook.add_recipes',compact('category','subcategory'));
    }

    /**
    * @method: selectCategory
    * @params: request data
    * @createddate: 18-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To show the sub-category according to selected category
    * @return:
    */
    public function selectCategory(Request $request){
        $category = Subcategory::where('category_id',$request->category)->get();
        return response()->json($category);
    }

     /**
    * @method: addRecipe
    * @params: request data
    * @createddate: 26-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To create a recipe
    * @return:
    */
    public function addRecipe(Request $request){
        if($request->isMethod('post')) {
            $validator = Validator::make($request->all(),[
                'category'=>'required',
                'subcategory'=> 'required',
                'name'=> 'required|string|min:3|max:40|unique:recipes',
                'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'ingrediants'=>'required|string',
                'preparation'=>'required|string'
            ]);
            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }
            $recipe = new Recipe;
            $recipe->name = $request->name;
            $recipe->category_id = $request->category;
            $recipe->subcategory_id = $request->subcategory;
            $recipe->ingrediants = $request->ingrediants;
            $recipe->preparation = $request->preparation;
            $recipe->user_id = Auth::id();
            $recipe->save();
            if($request->hasFile('recipe_pic')) {
                foreach($request->file('recipe_pic') as $image) {
                    $filename = $image->getClientOriginalName();
                    $image->move( public_path().'/frontend/images/', $filename);
                    $image = new RecipeImage;
                    $image->recipe_pic = $filename;
                    $recipe->pics()->save($image);
                }
            }
            return back()->with('success','CookRecipe added successfully and under review');
        }
    }

     /**
    * @method: editRecipe
    * @params: $id
    * @createddate: 26-07-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To edit a recipe
    * @return:
    */
    public function editRecipes($id){
        $id = Helper::decryptId($id);
        $recipe = Recipe::findOrFail($id);
        $category = Category::all();
        $selectedCategoryId = Recipe::where('id', $id)->pluck('category_id');
        $subcategory = Subcategory::where('category_id', $selectedCategoryId)->get();
        return view('Cook.edit_recipes',compact('recipe','category','subcategory'));
    }

    public function updateRecipe(Request $request,$id){
        $id = Helper::decryptId($id);
        $validator = Validator::make($request->all(),[
            'category'=>'required',
            'subcategory'=>'required',
            'name'=> 'required|string|min:3|max:40|unique:recipes,name,'.$id,
            'recipe_pic.*'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ingrediants'=>'required',
            'preparation'=>'required'
        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $recipe = Recipe::find($id);
        $recipe->name = $request->name;
        $recipe->category_id = $request->category;
        $recipe->subcategory_id = $request->subcategory;
        $recipe->ingrediants = $request->ingrediants;
        $recipe->preparation = $request->preparation;
        $recipe->save();
        if($request->hasFile('recipe_pic')) {
        $pic = RecipeImage::where('recipe_id', $id)->delete();
            foreach($request->file('recipe_pic') as $image) {
                $filename = $image->getClientOriginalName();
                $image->move( public_path().'/frontend/images/', $filename);
                $pic = new RecipeImage;
                $pic->recipe_id = $id;
                $pic->recipe_pic = $filename;
                $pic->save();              
            }
        }
        return back()->with('success','CookRecipe update successfully');
    }

}
