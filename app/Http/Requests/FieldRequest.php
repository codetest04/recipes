<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|min:3|max:30',
            'email'=> ['required',
                        'regex:/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/','unique:users'],
            'password' => 'required|string|min:5|max:16',
            'confirm_password' => 'required|same:password',
            'role'=>'required'
        ];
    }

    public function messages(){
        return [
            'password.regex'=> 'password must include atleast one upper case,atleast one lower case,atleast one number,atleast one special character includes $,!,%#'
        ];
    }
}
