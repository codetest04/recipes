<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteRecipe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','recipe_id',
    ];

}
