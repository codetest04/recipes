<?php

namespace App;
use Illuminate\Http\Request;
use App\Category;
use App\Recipe;

class Helper {

	/**
	* @method: encryptId
	* @params: text
	* @createddate: 19-07-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: to encrypt the id
	* @return: returns encrypted text
	*/
	public static function encryptId($id = null) {
		if ($id) {
			return $encrypted_string = str_replace('/', '@@@', openssl_encrypt($id, "AES-128-ECB", "!@%^&#*asdfghjklpsdfsdfoiuytrewqzxcvbnm"));
		}
		return false;
	}

	/**
	* @method: decryptId
	* @params: text
	* @createddate: 19-07-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: to decrypt the id
	* @return: returns decrypted text
	*/
	public static function decryptId($encrypted_string = null) {
		if ($encrypted_string) {
			$encrypted_string = str_replace('@@@', '/', $encrypted_string);
			return $decrypted_string = openssl_decrypt($encrypted_string, "AES-128-ECB", "!@%^&#*asdfghjklpsdfsdfoiuytrewqzxcvbnm");
		}
		return false;
	}

	/**
	* @method: getRecipeCategory
	* @params: text
	* @createddate: 19-07-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: to get the get recipe ategory
	* @return: returns category
	*/
	public static function getRecipeCategory(){
		$category = Category::all();
		return $category;
	}

	/**
	* @method: getSubcategoryList
	* @params: text
	* @createddate: 19-07-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: to get the get recipe ategory
	* @return: returns succategory
	*/
	public static function getSubcategoryList($categoryId){
        $subCategory = Subcategory::where('category_id',$categoryId)->get();
        return $subCategory;
    }

    /**
	* @method: searchRecipe
	* @params: text
	* @createddate: 19-07-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: to search particular recipe based on recipe name
	* @return:
	*/
    public static function searchRecipe($keyword){
        $recipe = Recipe::with(['pics','likedrecipes'])->where([['name', 'LIKE',"%$keyword%"],['status',1]])->get();
        return $recipe;
    }

}

?>