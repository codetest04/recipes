<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeImage extends Model
{
	protected $fillable = ['name'];
	
	public function recipe(){
		return $this->belongsTo('App\Recipe');
	}
}
