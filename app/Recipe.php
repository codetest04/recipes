<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Recipe extends Model
{
	public function pics() {
    	return $this->hasMany('App\RecipeImage');
	}
	public function category(){
		return $this->belongsTo('App\Category');
	}
	public function subcategory(){
		return $this->belongsTo('App\Subcategory');
	}
	public function comments(){
		return $this->hasMany('App\Comment');
	}

	public function favourite(){
		return $this->hasMany('App\FavouriteRecipe');
	}

	public function likedrecipes(){
		return $this->hasMany('App\LikeRecipe');
	}
	use Sortable;

	public $sortable = ['id','recipe_name'];
	/*public static function boot() {
        parent::boot();

        static::deleting(function($recipe) { // before delete() method call this
             $recipe->comments()->delete();
             // do the rest of the cleanup...
        });
    }*/
}
