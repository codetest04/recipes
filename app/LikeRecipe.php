<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeRecipe extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','recipe_id',
    ];

}
