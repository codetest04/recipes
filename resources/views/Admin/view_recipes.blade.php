@extends('layouts.admin')

@section('header')
  <h2>View Recipe</h2>
@endsection

@section('content')
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col-8">
        <h3 class="mb-0">Recipe Detail</h3>
      </div>
    </div>
    <div class="card-body">
      <form>
      <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Category</label>
                    <input type="text" id="input-username" name="category" class="form-control form-control-alternative" value="{{ $recipe->category->category_name }}" disabled>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Subcategory</label>
                    <input type="text" id="input-username" name="subcategory" class="form-control form-control-alternative" value="{{ $recipe->subcategory->subcategory_name }}" disabled>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Name</label>
                <input type="text" id="input-username" class="form-control form-control-alternative" name="name" value="{{ $recipe->name }}" disabled>
              </div>
            </div>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Ingrediants</label>
            <textarea rows="4" name="ingrediants" class="form-control form-control-alternative" disabled>{{ $recipe->ingrediants }}</textarea>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Preparation</label>
            <textarea rows="4" name="preparation" class="form-control form-control-alternative" disabled>{{ $recipe->preparation }}</textarea>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <label>Recipe Images</label>
          <div class="form-group focused" id="preview">
              @foreach($recipe->pics as $pic)
				<img src="{{ asset('frontend/images/'.$pic->recipe_pic) }}" width="150px" height="auto">        
              @endforeach
          </div>
        </div>
        <hr class="my-4"> 
      </form>
    </div>
  </div>
@endsection