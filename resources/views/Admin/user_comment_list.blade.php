@extends('layouts.admin')

@section('header')
<h1>Comment List</h3>
  @endsection

  @section('content')
  <div class="card-header border-0">
   <div class="row align-items-center">
    <div class="col-4">
      <h3 class="mb-0">Comment List</h3>
    </div>
    @if(session('success'))
    <div class="col-4 text-center alert alert-success">
      {{ session('success')}}
    </div>
    @endif
    @if(session('danger'))
    <div class="col-4 text-center alert alert-danger">
      {{ session('danger')}}
    </div>
    @endif
  </div>
</div>
<div class="table-responsive">
  <table class="table align-items-center table-flush">
    <thead class="thead-light">
      <tr>
        <th scope="col">Comment</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($getComment as $list)
      <tr>
       <td>
        {{ $list->comment }}
      </td>
      <td class="text-left">
        <a href="{{ url('/admin/view-comments',['id'=>Helper::encryptId($list->id)]) }}"><i class="fa fa-eye"></i></a>
        <?php
        $icon = !empty($list->comment_status) ? 'fa fa-unlock-alt' : 'fa fa-lock';
        ?>
        <a href="{{ url('admin/comment-status',['id'=>Helper::encryptId($list->id)]) }}" title=""><i class="<?php echo $icon; ?>"></i> </a>
        <i class="fa fa-trash" data-toggle="modal" data-target="#exampleModal{{ $list->id}}"></i>
        </td>
      </tr>
        <div class="modal fade" id="exampleModal{{ $list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Confirmation Box</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form method="post" action="{{ url('admin/comment-delete',['id'=>Helper::encryptId($list->id)]) }}">
                      @method('DELETE')
                      {{ csrf_field() }}
                    <div class="modal-body">
                      Are You sure you really want to delete comment?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
      @endforeach
    </tbody>
  </table>
  
</div>
@endsection