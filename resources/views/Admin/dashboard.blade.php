@extends('layouts.admin')

@section('header')
<h1>Recipe List</h3>
  @endsection

  @section('content')
  <div class="card-header border-0">
   <div class="row align-items-center">
    <div class="col-4">
      <h3 class="mb-0">Recipe List</h3>
    </div>
    @if(session('success'))
    <div class="col-4 text-center alert alert-success">
      {{ session('success')}}
    </div>
    @endif
    @if(session('danger'))
    <div class="col-4 text-center alert alert-danger">
      {{ session('danger')}}
    </div>
    @endif
  </div>
</div>
<div class="table-responsive">
  <table class="table align-items-center table-flush">
    <thead class="thead-light">
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Ingrediants</th>
        <th scope="col">Preparation</th>
        <th scope="col">Recipe_Pics</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($rec_list as $list)
      <tr>
        <th scope="row">
          <div class="media align-items-center">
            <a href="#" class="avatar rounded-circle mr-3">
              <img alt="Image placeholder" src="{{ asset('frontend/images/'.@$list['pics'][0]->recipe_pic) }}">
            </a>
            <div class="media-body">
              <span class="mb-0 text-sm">{{ $list->name }}</span>
            </div>
          </div>
        </th>
        <td>
         {{ str_limit($list->ingrediants,10,'...') }}
       </td>
       <td>
        {{ str_limit($list->preparation,10,'...')}}
      </td>
      <td>
        @foreach($list->pics as $pic)
        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Ryan Tompson">
          <img alt="Image placeholder" src="{{ asset('frontend/images/'.$pic->recipe_pic) }}" class="rounded-circle">
        </a>
        @endforeach
      </td>
      <td class="text-left">
        <a href="{{ url('/admin/recipes',['id'=>Helper::encryptId($list->id)]) }}"><i class="fa fa-eye"></i></a>
        <a href="{{ url('/admin/recipes/edit',['id'=>Helper::encryptId($list->id)]) }}"><i class="fa fa-edit"></i></a>
          <i class="fa fa-trash" data-toggle="modal" data-target="#exampleModal{{ $list->id}}"></i>
           <?php
        $icon = !empty($list->status) ? 'fa fa-unlock-alt' : 'fa fa-lock';
        ?>
        <a href="{{ url('admin/recipe-status',['id'=>Helper::encryptId($list->id)]) }}" title=""><i class="<?php echo $icon; ?>"></i> </a>
        </td>
      </tr>
      <div class="modal fade" id="exampleModal{{ $list->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Confirmation Box</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form method="post" action="{{ url('admin/recipeDelete',['id'=>Helper::encryptId($list->id)]) }}">
                      @method('DELETE')
                      {{ csrf_field() }}
                    <div class="modal-body">
                      Are You sure you really want to delete {{ $list->name }} recipe?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
      @endforeach
    </tbody>
  </table>
  
</div>
@endsection