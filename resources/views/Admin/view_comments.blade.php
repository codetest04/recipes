@extends('layouts.admin')

@section('header')
  <h2>View UserComments</h2>
@endsection

@section('content')
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col-8">
        <h3 class="mb-0">Comment Detail</h3>
      </div>
    </div>
    <div class="card-body">
      <form>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>User Comment</label>
            <textarea rows="4" name="user_comment" class="form-control form-control-alternative" disabled>{{ $comment->comment }}</textarea>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <label>Comment Image</label>
          <div class="form-group focused" id="preview">
				<img src="{{ asset('frontend/images/'.$comment->comment_pic) }}" width="150px" height="auto">        
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <label>User rating</label>
          <div class="form-group focused" id="preview">
            @for($i=0; $i<$comment->rating; $i++)
            <i class="fa fa-star" aria-hidden="true"></i>
            @endfor
          </div>
        </div> 
      </form>
    </div>
  </div>
@endsection