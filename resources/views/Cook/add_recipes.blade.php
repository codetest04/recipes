@extends('layouts.cook')

@section('header')
  <h2>Add Recipe Form</h2>
@endsection

@section('content')
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col-4">
        <h3 class="mb-0">Recipe Detail</h3>
      </div>
      @if(session('success'))
      <div class="col-4 text-center alert alert-success">
        {{ session('success')}}
      </div>
      @endif
    </div>
    <div class="card-body">
      <form action="{{ url('/cook/addRecipe') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Category</label>
                <select name="category" id="add_category" class="form-control form-control-alternative">
                    <option value="">Select</option>
                    @foreach($category as $cat)    
                        <option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
                    @endforeach
                </select>
				    <span style="color:red;">{{ $errors->first('category') }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Subcategory</label>
                <select name="subcategory" id="add_subcategory" class="form-control form-control-alternative">
                    <option value="">Select</option>
                </select>
				    <span style="color:red;">{{ $errors->first('subcategory') }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Name</label>
                <input type="text" id="input-username" class="form-control form-control-alternative" name="name" placeholder="Recipe Name">
				    <span style="color:red;">{{ $errors->first('name') }}</span>
              </div>
            </div>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Ingrediants</label>
            <textarea rows="4" name="ingrediants" class="form-control form-control-alternative" placeholder="Enter Recipe Ingrediants"></textarea>
			<span style="color:red;">{{ $errors->first('ingrediants') }}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Preparation</label>
            <textarea rows="4" name="preparation" class="form-control form-control-alternative" placeholder="Enter Recipe Preparation"></textarea>
			<span style="color:red;">{{ $errors->first('preparation') }}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <label>Recipe Images</label>
          <div class="form-group focused" id="preview">
              
          </div>
          <input type="file" name="recipe_pic[]" id="recipe_image" multiple>
            <span style="color:red;">{{ $errors->first('recipe_pic') }}</span>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <button type="submit" class="btn btn-primary my-4">Submit</button>
          </div>
        </div>  
      </form>
    </div>
  </div>
@endsection