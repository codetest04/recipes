@extends('layouts.cook')

@section('header')
  <h2>Edit Recipe Form</h2>
@endsection

@section('content')
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col-4">
        <h3 class="mb-0">Recipe Detail</h3>
      </div>
      @if(session('success'))
      <div class="col-4 text-center alert alert-success">
        {{ session('success')}}
      </div>
      @endif
    </div>
    <div class="card-body">
      <form action="{{ url('/cook/updateRecipe',['id'=>Helper::encryptId($recipe->id)]) }}" method="post" enctype="multipart/form-data">
      @method('PATCH')  
      {{ csrf_field() }}
      <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Category</label>
                <select name="category" id="add_category" class="form-control form-control-alternative">
                    <option value="">Select</option>
                    @if(!empty($category))
                    @foreach($category as $cat)   
                    <?php
                    $selected="";
                    if($cat->id === $recipe->category_id){
                      $selected="selected";
                    }
                    ?> 
                        <option value="{{ $cat->id }}" {{ $selected }}>{{ $cat->category_name }}</option>
                    @endforeach
                    @endif
                </select>
                <span style="color:red;">{{ $errors->first('category') }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Subcategory</label>
                <select name="subcategory" id="add_subcategory" class="form-control form-control-alternative">
                    <option value="">Select</option>
                      @if(!empty($subcategory))
                      @foreach($subcategory as $subcat)
                      <?php
                        $select = "";
                        if($subcat->id===$recipe->subcategory_id){
                          $select = "selected";
                        }                   
                      ?>
                        <option value="{{ $subcat->id }}" {{ $select }}>{{ $subcat->subcategory_name }}</option>
                      @endforeach
                      @endif
                </select>
                <span style="color:red;">{{ $errors->first('subcategory') }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="pl-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group focused">
                <label class="form-control-label" for="input-username">Name</label>
                <input type="text" id="input-username" class="form-control form-control-alternative" name="name" value="{{ $recipe->name }}">
                <span style="color:red;">{{ $errors->first('name') }}</span>
              </div>
            </div>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Ingrediants</label>
            <textarea rows="4" name="ingrediants" class="form-control form-control-alternative">{{ $recipe->ingrediants }}</textarea>
                <span style="color:red;">{{ $errors->first('ingrediants') }}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <label>Preparation</label>
            <textarea rows="4" name="preparation" class="form-control form-control-alternative">{{ $recipe->preparation }}</textarea>
                <span style="color:red;">{{ $errors->first('preparation') }}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <label>Recipe Images</label>
          <div class="form-group focused" id="preview">
            @if(!empty($recipe))
              @foreach($recipe->pics as $pic)
                  <img src="{{ asset('frontend/images/'.$pic->recipe_pic) }}" width="150px" height="auto">        
              @endforeach
            @endif
          </div>
          <input type="file" name="recipe_pic[]" id="recipe_image" multiple>
            <span style="color:red;">{{ $errors->first('recipe_pic') }}</span>
        </div>
        <hr class="my-4">
        <div class="pl-lg-4">
          <div class="form-group focused">
            <button type="submit" class="btn btn-primary my-4">Update</button>
          </div>
        </div>  
      </form>
    </div>
  </div>
@endsection