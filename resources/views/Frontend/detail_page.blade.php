@extends('layouts.frontend')

@section('content')
<div class="services-breadcrumb">
	<div class="agile_inner_breadcrumb">
		<div class="container">
			<ul class="w3_short">
				<li>
					<a href="{{ url('/') }}">Home</a>
					<i>|</i>
				</li>
				<li>Recipe Info</li>
			</ul>
			@if(session('success'))
			<div class="alert alert-success" style="text-align: center;">
				{{ session('success') }}
			</div>
			@endif
		</div>
	</div>
</div>
<div class="banner-bootom-w3-agileits">
	<div class="container" id="divToPrint">
		<h3 class="tittle-w3l">Recipe Info
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<div class="col-md-5 single-right-left ">
			<div class="grid images_3_of_2">
				<div class="flexslider">
					<ul class="slides">
						@if(!empty($rec->pics))
						@foreach($rec->pics as $pic)
						<li data-thumb="{{ asset('frontend/images/'.$pic->recipe_pic) }}">
							<div class="thumb-image">
								<img src="{{ asset('frontend/images/'.$pic->recipe_pic) }}" data-imagezoom="true" class="img-responsive" alt="">
							</div>
						</li>
						@endforeach
						@endif
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-7 single-right-left simpleCart_shelfItem">
			<h3>{{ $rec->name }}</h3>
			@if(!empty(count($comment)))
			<?php
				$leng = $comment->count('rating');
				$rate = $comment->sum('rating')/$leng;
				$user_rating = round($rate);
			?>
			<div>
				<span>
					@for($i=1; $i<=$user_rating; $i++)
					<img src="{{ asset('frontend/images/star22.png') }}">
					@endfor
				</span>
			</div>
			@endif
			<p>
				<label>Ingrediants</label>
			</p>
			<div class="single-infoagile">
				<ul>
					<li>
						{{ $rec->ingrediants}}
					</li>
				</ul>
			</div>
			<div class="product-single-w3l">
				<p><i class="fa fa-hand-o-right" aria-hidden="true"></i>
				<label>Preparation</label></p>
				<ul>
					<li>
						{{ $rec->preparation }}
					</li>
				</ul>
			</div>
			<div class="product-single-w3l">
				<p><i class="fa fa-hand-o-right" aria-hidden="true"></i>
				<label><a href="{{ url('print-recipe',['id'=>Helper::encryptId($rec->id)]) }}">Print Recipe in Pdf</a></label></p>
			</div>
			<button id="pdf_file">Pdf generate</button>
		</div>
	</div>
	
	<div class="container">
		<h3 class="tittle-w3l">Comment Section
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<div class="contact agileits">
			<div class="contact-agileinfo">
				@auth
				<div class="contact-form wthree">
					<form action="{{ url('/usercomments') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="recipe_id" value="{{ Helper::encryptId($rec->id) }}">
						<div class="styled-input">
							<textarea class="textarea" rows="4" placeholder="Enter comment" name="comment_name">{{ old('comment_name') }}</textarea>
							<span style="color:red;">{{ $errors->first('comment_name') }}</span>
						</div><br>
						<div class="style-input">
							<input type="text" placeholder="rating" name="rating" min=1 max=5 value="{{ old('rating') }}">
							<span style="color:red;">{{ $errors->first('rating') }}</span>
						</div><br>
						<div class="styled-input">
							<input type="file" name="comment_pic">
							<span style="color:red;">{{ $errors->first('comment_pic') }}</span>
						</div>
						<input type="submit" value="Submit">
					</form>
				</div>
				@endauth
				<br><br>
				@if(!empty(count($comment)))
				<div class="contact-form wthree">
					<div>
						@foreach($comment as $value)
						<label></label>
						<textarea disabled>{{ $value->users->name }}:{{ $value->comment }}
						</textarea>
						 @for($i=0; $i<$value->rating; $i++)
							<img src="{{ asset('frontend/images/star22.png') }}">
            			@endfor
						@endforeach
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection