@extends('layouts.frontend')

@section('content')
<!-- top Products -->
    <div class="ads-grid">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Favourite List
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			@if(session('success'))
				<div class="alert alert-success">
					{{ session('success') }}
				</div>
			@endif
			@if(session('danger'))
				<div class="alert alert-danger">
					{{ session('danger') }}
				</div>
			@endif
			<!-- //tittle heading -->
			<!-- product right -->
			<div class="agileinfo-ads-display col-md-12">
				<div class="wrapper">
					<!-- first section (nuts) -->
					<div class="product-sec1">
						<h3 class="heading-tittle"></h3>
						@if(!empty(count($userFavourite)))
						@foreach($userFavourite as $favourite)
						<div class="col-md-4 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									@foreach($favourite->pics as $pic)
									@if($loop->last)
									<a href="{{ url('/recipedetail',['id'=>Helper::encryptId($favourite->id)]) }}"><img src="{{ asset('frontend/images/'.@$pic->recipe_pic) }}" width="50%" height="auto" alt="" style="display:inline'"></a>
									@endif
									@endforeach
									@auth
									@if(!empty($favourite->likedrecipes))
									@foreach($favourite->likedrecipes as $like)
									<?php
									$icon ="";
										if($like->user_id!==Auth::id()){
											$icon = 'product-new-top';
										}
										else if($like->user_id==Auth::id()) {
											$icon = 'product-new-top-fav';
										}
									?>
									<a href="{{ url('/likerecipe',['id'=>Helper::encryptId($favourite->id)]) }}"><span class="<?php echo $icon; ?> likerecipe"></span></a>

									@endforeach
									@endif
									@if(empty(count($favourite->likedrecipes)))
									<a href="{{ url('/likerecipe',['id'=>Helper::encryptId($favourite->id)]) }}"><span class="product-new-top likerecipe"></span></a>
									@endif
									@endauth
								</div>
								<div class="item-info-product ">
									<h4>
										<a href="{{ url('/recipedetail',['id'=>Helper::encryptId($favourite->id)]) }}">{{ $favourite->name}}</a>
									</h4>
									<div class="info-product-price">
										<span class="item_price">
											<?php
											$recipe_likes = count($favourite->likedrecipes);
												$icon = $recipe_likes>0 ? "fa fa-thumbs-o-up" : "fa fa-thumbs-o-down"
											?>
											<i class="<?php echo $icon;?>"></i>
										</span>
										<span>{{ $recipe_likes }}</span>
									</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
										<a href="{{ url('/remove_favourite-list',['id'=>Helper::encryptId($favourite->id)]) }}"><input type="submit" name="submit" value="Remove from favourite list" class="button" /></a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						@else
							<p style="text-align: center;">No data found</p>
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection