@extends('layouts.frontend')

@section('content')
<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="{{ url('/') }}">Home</a>
						<i>|</i>
					</li>
					<li>Add Recipe</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->
	<!-- contact page -->
	<div class="contact-w3l">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Add Recipe
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
            </h3>
            @if(session('success'))
            <div class="alert alert-success" id="success">
                {{ session('success')}}
            </div>
            @endif
			<!-- //tittle heading -->
			<!-- contact -->
			<div class="contact agileits">
				<div class="contact-agileinfo">
					<div class="contact-form wthree">
                        <form action="{{ url('/recipes') }}" method="post" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="">
								<label>Category</label>
								<select name="category" class="form-control sel">
									<option></option>
								</select>
								<span style="color:red;">{{ $errors->first('category') }}</span>
							</div>
							<div class="">
							<label>Subcategory</label>
								<select name="subcategory" class="form-control sel">
									<option></option>
								</select>
								<span style="color:red;">{{ $errors->first('subcategory') }}</span>
							</div>
							<div class="">
								<input type="text" name="name" placeholder="Name" value="{{ old('name')}}" required>
								<span style="color:red;">{{ $errors->first('name') }}</span>
							</div>
							<div class="">
								<textarea placeholder="Ingrediants" name="ingrediants" required>{{ old('name') }}</textarea>
								<span style="color:red;">{{ $errors->first('ingrediants') }}</span>
							</div>
							<div class="">
								<textarea placeholder="Preparation" name="preparation" required>{{ old('name') }}</textarea>
								<span style="color:red;">{{ $errors->first('preparation') }}</span>
							</div>
                            <div class="">
								<input type="file" placeholder="recipe Image" id="recipe_image" name="recipe_pic[]" multiple>
								<span style="color:red;">{{ $errors->first('recipe_pic') }}</span>
							</div>
                            <div id="preview"> 
                            </div>
							<input type="submit" value="Submit">
						</form>
					</div>
				</div>
			</div>
			<!-- //contact -->
		</div>
    </div>
@endsection