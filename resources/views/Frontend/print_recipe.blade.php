<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  	<table>
  		<caption><h3>{{ $recipe->name }}</h3></caption> 
  		<thead> 
  			<tr> 
  				<td colspan="4"> <strong>Ingrediansts</strong><br>{{ $recipe->ingrediants}}</td>
  			</tr>
  			<tr> 
  				<td colspan="4"> <strong>Preparation</strong><br>{{ $recipe->preparation}}</td>
  			</tr> 
        <tr>
          @foreach($recipe->pics as $pic)
          <td colspan="2"><img src="{{ public_path().'/frontend/images/'.$pic->recipe_pic }}" width="30%" height="auto"></td>
          @endforeach
        </tr>
  		</thead> 
  		<tbody>
  			
  		</tbody>
  	</table>

  </body>
</html>

