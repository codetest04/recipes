@extends('layouts.frontend')

@section('content')
<!-- top Products -->
    <div class="ads-grid">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Our Top Products
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			@if(session('success'))
				<div class="alert alert-success">
					{{ session('success') }}
				</div>
			@endif
			@if(session('danger'))
				<div class="alert alert-danger">
					{{ session('danger') }}
				</div>
			@endif
			<!-- //tittle heading -->
			<!-- product right -->
			<div class="agileinfo-ads-display col-md-12">
				<div class="wrapper">
					<!-- first section (nuts) -->
					<div class="product-sec1">
						<h3 class="heading-tittle"></h3>
						@if(!empty($recipes))
						@foreach($recipes as $recipe)
						<div class="col-md-4 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									@if(!empty($recipe->pics))
									@foreach($recipe->pics as $pic)
									@if($loop->last)
									<a href="{{ url('/recipedetail',['id'=>Helper::encryptId($recipe->id)]) }}"><img src="{{ asset('frontend/images/'.@$pic->recipe_pic) }}" width="50%" height="auto" alt="" style="display:inline'"></a>
									@endif
									@endforeach
									@endif
									@auth
									@if(!empty($recipe->likedrecipes))
									@foreach($recipe->likedrecipes as $like)
									<?php
									$icon ="";
										if($like->user_id!==Auth::id()){
											$icon = 'product-new-top';
										}
										else if($like->user_id==Auth::id()) {
											$icon = 'product-new-top-fav';
										}
									
									?>
									<a href="{{ url('/likerecipe',['id'=>Helper::encryptId($recipe->id)]) }}"><span class="<?php echo $icon; ?> likerecipe"></span></a>

									@endforeach
									@endif
									@if(empty(count($recipe->likedrecipes)))
									<a href="{{ url('/likerecipe',['id'=>Helper::encryptId($recipe->id)]) }}"><span class="product-new-top likerecipe"></span></a>
									@endif
									@endauth
								</div>
								<div class="item-info-product ">
									<h4>
										<a href="{{ url('/recipedetail',['id'=>Helper::encryptId($recipe->id)]) }}">{{ $recipe->name}}</a>
									</h4>
									<div class="info-product-price">
										<span class="item_price">
											<?php
											$recipe_likes = count($recipe->likedrecipes);
												$icon = $recipe_likes>0 ? "fa fa-thumbs-o-up" : "fa fa-thumbs-o-down"
											?>
											<i class="<?php echo $icon;?>"></i>
										</span>
										<span>{{ $recipe_likes }}</span>
									</div>

									<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
										<a href="{{ url('/add_favourite-list',['id'=>Helper::encryptId($recipe->id)]) }}"><input type="submit" name="submit" value="Add to favourite list" class="button" /></a>
									</div>

								</div>
							</div>
						</div>
						@endforeach
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection