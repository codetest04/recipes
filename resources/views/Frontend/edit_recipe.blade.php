@extends('layouts.frontend')

@section('content')
<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="{{ url('/') }}">Home</a>
						<i>|</i>
					</li>
					<li>Update Recipe</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->
	<!-- contact page -->
	<div class="contact-w3l">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Update Recipe
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
            </h3>
            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success')}}
            </div>
            @endif
			<!-- //tittle heading -->
			<!-- contact -->
			<div class="contact agileits">
				<div class="contact-agileinfo">
					<div class="contact-form wthree">
                        <form action="{{ url('/recipes/',['id'=>Helper::encryptId($recipe->id)]) }}" method="post" enctype="multipart/form-data">
                            @method('PATCH')
                            {{ csrf_field() }}
							<div class="">
								<input type="text" name="name" placeholder="Name" value="{{ $recipe->name }}" required="">
								<span style="color:red;">{{ $errors->first('name') }}</span>
                            </div>
							<div class="">
								<textarea placeholder="Ingrediants" name="ingrediants" required="">{{ $recipe->ingrediants }}</textarea>
								<span style="color:red;">{{ $errors->first('ingrediants') }}</span>
                            </div>
							<div class="">
								<textarea placeholder="Preparation" name="preparation" required="">{{ $recipe->preparation }}</textarea>
								<span style="color:red;">{{ $errors->first('preparation') }}</span>
                            </div>
                            <div class="">
                                <div class="flexslider" id="edit_preview">
                                    <ul class="slides">
                                        @foreach($recipe->pics as $pic)
                                        <li data-thumb="{{ asset('frontend/images/'.$pic->recipe_pic) }}">
                                        </li>
                                    @endforeach
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
								<input type="file" placeholder="recipe Image" accept="image/*"  id="edit_recipeimage" name="recipe_pic[]" multiple>
								<span style="color:red;">{{ $errors->first('recipe_pic') }}</span>
                            </div>
							<input type="submit" value="Submit">
						</form>
					</div>
				</div>
			</div>
			<!-- //contact -->
		</div>
    </div>
@endsection