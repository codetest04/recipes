<!--   Core   -->
  <script src="{{ asset('adm/assets/js/plugins/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('adm/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!--   Optional JS   -->
  <script src="{{ asset('adm/assets/js/plugins/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('adm/assets/js/plugins/chart.js/dist/Chart.extension.js') }}"></script>
  <!--   Argon JS   -->
  <script src="{{ asset('adm/assets/js/argon-dashboard.min.js') }}"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
  <script>
      $(document).ready(function(){
        $('.alert-success,.alert-danger').fadeOut(4000);
      });
</script>
  <script>
        $(document).ready(function () {
            $("#recipe_image").change(function () {
              $('#preview').empty();
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script>
      $('#add_category').on('change',function(){
        var category = $(this).val();
        console.log(category);
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url:'/admin/category_match',
          type:'post',
          data:{
            category,
          },
          success:function(data){
            $('#add_subcategory').empty();
            console.log(data);
            $.each(data,function(key,value){
              console.log(value.subcategory_name);
              $('#add_subcategory').append('<option value='+value.id+'>'+value.subcategory_name+'</option>');
            });
            
          },
          error:function(data){
            console.log(data);
          }
        })

      })
    </script>
   