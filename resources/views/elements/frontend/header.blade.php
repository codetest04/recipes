<div class="header-most-top">
	<p>Grocery Offer Zone Top Deals & Discounts</p>
</div>
    <div class="header-bot">
    	<div class="header-bot_inner_wthreeinfo_header_mid">
    		<div class="col-md-4 logo_agile">
    			<h1>
    				<a href="{{ url('/') }}">
    					<span>G</span>rocery
    					<span>S</span>hoppy
    					<img src="{{ asset('frontend/images/logo2.png') }}" alt=" ">
    				</a>
    			</h1>
    		</div>
    		<div class="col-md-8 header">
                <ul>
                    @guest('web')
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal1" class="mdl_clr">
                            <span class="fa fa-unlock-alt" aria-hidden="true"></span> Sign In </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal2" class="mdl_clr">
                            <span class="fa fa-pencil-square-o" aria-hidden="true"></span> Sign Up </a>
                    </li>
                    @endguest
                    @auth
                    <li>
                        <span class="fa fa-pencil-square-o" aria-hidden="true"></span> {{ Auth::guard('web')->user()->name }} 
                    </li>
                        <a href="{{ url('/logout') }}"><span class="fa fa-sign-out" aria-hidden="true"></span>Logout</a>
                    <li>
                    @endauth
                </ul>
    			<!-- search -->
    			<div class="agileits_search">
    				<form action="{{ url('/') }}" method="post" id="searchRecipe">
    					{{ csrf_field() }}
    					<input name="search" type="search" placeholder="Search Recipe Here" value="{{ request('search') }}" id="search" required>
    					<button type="submit" class="btn btn-default" aria-label="Left Align">
    						<span class="fa fa-search" aria-hidden="true"> </span>
    					</button>
    				</form>
    			</div>
    			<div class="clearfix"></div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    </div>
    <!-- Signup Modal-->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body modal-body-sub_agile">
                    <div class="main-mailposi">
                    </div>
                    <div class="modal_body_left modal_body_left1">
                        <h3 class="agileinfo_sign">Sign Up</h3>
                        <p>
                         
                        </p>
                        <form action="{{ url('/register') }}" method="post" id="user_register">
                            {{ csrf_field() }}
                            <div class="styled-input agile-styled-input-top">
                                <input type="text" placeholder="Name" name="name" value="{{ old('name') }}">
                            <span style="color:red;" class="err" id="name_error"></span>
                            </div>
                            <div class="styled-input">
                                <input type="email" placeholder="E-mail" name="email" value="{{ old('email') }}">
                            <span style="color:red;" class="err" id="email_error"></span>
                            </div>
                            <div class="styled-input">
                                <input type="password" placeholder="Password" name="password" id="password1">
                            <span style="color:red;" class="err" id="pass_error"></span>
                            </div>
                            <div class="styled-input">
                                <input type="password" placeholder="Confirm Password" name="confirm_password" id="password2">
                            <span style="color:red;" class="err" id="confirm_pass_error"></span>
                            </div>
                            <div class="styled-input">
                                <select name="role" class="textarea">
                                    <option value="" selected>Select Role</option>
                                    <option value="{{ Crypt::encrypt(0) }}">User</option>
                                    <option value="{{ Crypt::encrypt(1) }}">Cook</option>
                                </select>
                            <span style="color:red;" class="err" id="role_error"></span>
                            </div>
                            <input type="submit" value="Sign Up">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sign in -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body modal-body-sub_agile">
                    <div class="main-mailposi">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span>
                    </div>
                    <div class="modal_body_left modal_body_left1">
                        <h3 class="agileinfo_sign">Sign In </h3>
                        <p>
                            Sign In now, Let's start your Grocery Shopping.
                        </p>
                        <form action="{{ url('login') }}" method="post" id="user_login">
                            {{ csrf_field() }}
                            <div class="styled-input agile-styled-input-top">
                                <input type="email" placeholder="Email" name="email" required>
                            </div>
                             <span style="color:red;" class="err" id="login_email_err"></span>
                            <div class="styled-input">
                                <input type="password" placeholder="Password" name="password" required>
                                <span style="color:red;" class="err" id="login_pass_err"></span>
                            </div>
                            <div class="styled-input">
                                <span style="color:red;" class="err" id="credential_err"></span>
                            </div>
                            <input type="submit" value="Sign In">
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- navigation -->
    <div class="ban-top">
    	<div class="container">
    		<div class="top_nav_left">
    			<nav class="navbar navbar-default">
    				<div class="container-fluid">
    					<div class="navbar-header">
    						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
    						aria-expanded="false">
    						<span class="sr-only">Toggle navigation</span>
    						<span class="icon-bar"></span>
    						<span class="icon-bar"></span>
    						<span class="icon-bar"></span>
    					</button>
    				</div>
    				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
    					<ul class="nav navbar-nav menu__list">
    						<li class="active">
    							<a class="nav-stylehead" href="{{ url('/') }}">Home
    								<span class="sr-only">(current)</span>
    							</a>
    						</li>
    						@if(!empty(Helper::getRecipeCategory()))
    						@foreach(Helper::getRecipeCategory() as $cat)
    						<li class="dropdown">
    							<a href="#" class="dropdown-toggle nav-stylehead " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $cat->category_name }}
    								<span class="caret"></span>
    							</a>
    							<?php $subCate= Helper::getSubcategoryList($cat->id);?>

    							<ul class="dropdown-menu multi-column columns-3">
    								<div class="agile_inner_drop_nav_info">
    									<div class="col-sm-4 multi-gd-img">
    										<ul class="multi-column-dropdown">
    											@if(!empty($subCate))
    											@foreach($subCate as $val)
    											<li>
    												<a href="{{ url('/recipelist',['id'=>Helper::encryptId($val->id)]) }}">{{$val->subcategory_name}}</a>
    											</li>
    											@endforeach
    											@endif
    										</ul>
    									</div>
    									<div class="col-sm-4 multi-gd-img">
    										<img src="{{ asset('frontend/images/category.jpeg') }}" alt="">
    									</div>
    									<div class="clearfix"></div>
    								</div>
    							</ul>
    						</li>
    						@endforeach
    						@endif
                            @auth
                            <li>
                                <a href="{{ url('favourite-list') }}">Favourite List</a>                   
                            </li>
                            @endauth
    					</ul>
    				</div>
    			</div>
    		</nav>
    	</div>
    </div>
</div>
	<!-- //navigation -->