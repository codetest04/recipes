<!-- js-files -->
	<!-- jquery -->
	<script src="{{  asset('frontend/js/jquery-2.1.4.min.js') }}"></script>
	<!-- //jquery -->

	<!-- popup modal (for signin & signup)-->
	<script src="{{  asset('frontend/js/jquery.magnific-popup.js') }}"></script>
	<script>
		var SITE_URL='<?php echo url('/'); ?>';
		$(document).ready(function () {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>
	<!-- Large modal -->
	<!-- <script>
		$('#').modal('show');
	</script> -->
	<!-- //popup modal (for signin & signup)-->

	<!-- cart-js -->
	<script src="{{  asset('frontend/js/minicart.js') }}"></script>
	<script>
		paypalm.minicartk.render(); //use only unique class names other than paypalm.minicartk.Also Replace same class name in css and minicart.min.js

		paypalm.minicartk.cart.on('checkout', function (evt) {
			var items = this.items(),
				len = items.length,
				total = 0,
				i;

			// Count the number of each item in the cart
			for (i = 0; i < len; i++) {
				total += items[i].get('quantity');
			}

			if (total < 3) {
				alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
				evt.preventDefault();
			}
		});
	</script>
	<!-- //cart-js -->

	<!-- price range (top products) -->
	<script src="{{  asset('frontend/js/jquery-ui.js') }}"></script>
	<script>
		//<![CDATA[ 
		$(window).load(function () {
			$("#slider-range").slider({
				range: true,
				min: 0,
				max: 9000,
				values: [50, 6000],
				slide: function (event, ui) {
					$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
				}
			});
			$("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

		}); //]]>
	</script>
	<!-- //price range (top products) -->
	<!-- FlexSlider -->
	<script src="{{ asset('frontend/js/jquery.flexslider.js') }}"></script>
	<script>
		// Can also be used with $(document).ready()
		$(window).load(function () {
			$('.flexslider').flexslider({
				animation: "slide",
				controlNav: "thumbnails"
			});
		});
	</script>
	<!-- //FlexSlider-->
	<!-- imagezoom -->
	<script src="{{ asset('frontend/js/imagezoom.js') }}"></script>
	<!-- //imagezoom -->
	<!-- flexisel (for special offers) -->
	<script src="{{  asset('frontend/js/jquery.flexisel.js') }}"></script>
	<script>
		$(window).load(function () {
			$("#flexiselDemo1").flexisel({
				visibleItems: 3,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: {
					portrait: {
						changePoint: 480,
						visibleItems: 1
					},
					landscape: {
						changePoint: 640,
						visibleItems: 2
					},
					tablet: {
						changePoint: 768,
						visibleItems: 2
					}
				}
			});

		});
	</script>
	<script src="{{  asset('frontend/js/move-top.js') }}"></script>
	<script src="{{  asset('frontend/js/easing.js') }}"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<script src="{{  asset('frontend/js/bootstrap.js') }}"></script>
	<script>
      $(document).ready(function(){
        $('.alert-success,.alert-danger').fadeOut(4000);
      });
	</script>
	<script>
        $(document).ready(function () {
            $("#recipe_image").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
	<script>
		$('#edit_recipeimage').change(function(){
			if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#edit_preview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
		});
	</script>
	<script>
		$(document).ready(function(){
			$('#user_register').submit(function(event){
				event.preventDefault();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
					}
				});
				var formdata = new FormData(this);
				$.ajax({
					type: 'post',
					url: 'register',
					data:formdata,
	                success:function(data){
	                    if(data.role_id==0){
	                    	window.location.href="/";
	                    }
	                    if(data.role_id==1){
	                    	window.location.href="cook/index";
	                    }
	                },
	                 processData: false,
                  	contentType: false,
	                error:function(data){
	                	var error = data.responseJSON.errors;
	                	console.log(error);
	                	if(error){
	                		$('.err').empty();
	                		if(error.confirm_password){
	                			$('#confirm_pass_error').append(error.confirm_password);
	                		}
	                		if(error.name){
	                			$('#name_error').append(error.name);
	                		}
	                		if(error.email){
	                			$('#email_error').append(error.email);
	                		}
	                		if(error.password){
	                			$('#pass_error').append(error.password);
	                		}
	                		if(error.role){
	                			$('#role_error').append(error.role);
	                		}
	                	}
	                }  
	             });
	        });
		});
	</script>
	<script>
		$(document).ready(function(){
			$('#user_login').submit(function(event){
				event.preventDefault();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
					}
				});
				var formdata = new FormData(this);
				$.ajax({
					type: 'post',
					url: '/login',
					data:formdata,
	                success:function(data){
	                	console.log(data);
	                    if(data==0){
	                    	window.location.href="/";
	                    }
	                    else if(data==1){
	                    	window.location.href="cook/index";
	                    }
	                    else {
	                    	window.location.href="/";
	                    }
	                },
	                 processData: false,
                  	contentType: false,
	                error:function(data){
	                	if(data.responseJSON){
	                		$('#credential_err').empty();
	                		$('#credential_err').append(data.responseJSON);
	                	}
	                	var error = data.responseJSON.errors;
	                	if(error){
	                		$('.err').empty();
	                		if(error.email){
	                			$('#login_email_err').append(error.email);
	                		}
	                		if(error.password){
	                			$('#login_pass_err').append(error.password);
	                		}
	                	}
	                }  
	             });
	        });
		});
	</script>
	<script type="text/javascript">
		$('.close').on('click',function(){
			$('#user_login')[0].reset();
			$('#user_register')[0].reset();
			$('.err').empty();
		});
	</script>
	<script type="text/javascript">
		$(document).keyup(function(e) {
  			if (e.keyCode === 27) {
  				$('#user_login')[0].reset();
				$('#user_register')[0].reset();
				$('.err').empty();
			}
		});
	</script>
	<script>
		$('.mdl_clr').on('click',function(){
			$('#user_login')[0].reset();
			$('#user_register')[0].reset();
			$('.err').empty();
		});
	</script>
	  <script>
      $(document).ready(function(){
        $('.alert-success,.alert-danger').fadeOut(4000);
      });
	</script>
	<script>
		$('#pdf_file').on('click',function(){
			var win = window.open('','','left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');
			    var content = "<html>";
			    content += "<body onload=\"window.print(); window.close();\">";
			    content += document.getElementById("divToPrint").innerHTML ;
			    content += "</body>";
			    content += "</html>";
			    win.document.write(content);
			    win.document.close();
		});
	</script>




