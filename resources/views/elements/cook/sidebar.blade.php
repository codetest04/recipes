<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="{{ url('cook/index') }}">
        <img src="{{ asset('adm/assets/img/brand/blue.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item active">
          <a class=" nav-link active " href="{{ url('/cook/index') }}"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{ url('/cook/addRecipe') }}">
              <i class="ni ni-single-02 text-yellow"></i> Add Recipe
            </a>
          </li>
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->
        <!-- <h6 class="navbar-heading text-muted">Documentation</h6> -->
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
          <!-- <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
              <i class="ni ni-spaceship"></i> Getting started
            </a>
          </li> -->
        </ul>
      </div>
    </div>
  </nav>