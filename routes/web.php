<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get','post'],'/','User\UserController@index');

Route::post('/subcategorylist/{id}','User\UserController@subcategoryList');

Route::get('/recipelist/{id}','User\UserController@recipeList');

Route::get('/recipedetail/{id}','User\UserController@recipedetail');

Route::match(['get'],'/admin','Admin\AdminLoginController@getLogin');

Route::post('register','User\UserLoginController@register');

Route::post('login','User\UserLoginController@login');

Route::get('logout','User\UserLoginController@logout');

Route::get('add_favourite-list/{id}','User\UserController@addFavouriteRecipeList')->middleware('auth:web');

Route::get('remove_favourite-list/{id}','User\UserController@removeFavouriteRecipeList')->middleware('auth:web');

Route::get('favourite-list','User\UserController@showFavouriteRecipeList')->middleware('auth:web');

Route::get('likerecipe/{id}','User\UserController@likeRecipe')->middleware('auth:web');

Route::get('print-recipe/{id}','User\UserController@printRecipe');
Route::get('/cookrecipes','User\UserController@cookRecipes');
Route::post('usercomments','User\UserController@userComments');

Route::prefix('admin')->group(function(){
    Route::get('/index','Admin\AdminController@index');
    Route::get('/login','Admin\AdminLoginController@getLogin');
    Route::post('/login','Admin\AdminLoginController@login');
    Route::get('/logout','Admin\AdminLoginController@logout');
    Route::get('/recipes/{id}','Admin\AdminController@showRecipes');
    Route::get('/recipes/edit/{id}','Admin\AdminController@editRecipes');
    Route::patch('/updateRecipe/{id}','Admin\AdminController@updateRecipe');
    Route::get('/addRecipe','Admin\AdminController@addRecipeForm');
    Route::post('/addRecipe','Admin\AdminController@addRecipe');
    Route::post('/category_match','Admin\AdminController@selectCategory');
    Route::delete('recipeDelete/{id}','Admin\AdminController@deleteRecipe');
    Route::get('user-comments','Admin\AdminController@getUserComments');
    Route::get('comment-status/{id}','Admin\AdminController@commentStatus');
    Route::get('view-comments/{id}','Admin\AdminController@viewComments');
    Route::delete('comment-delete/{id}','Admin\AdminController@deleteComment');
    Route::get('recipe-status/{id}','Admin\AdminController@recipeStatus');
});

Route::prefix('cook')->group(function(){
    Route::get('index','Cook\CookController@index')->middleware('cook');
    Route::get('/addRecipe','Cook\CookController@addRecipeForm');
    Route::post('/addRecipe','Cook\CookController@addRecipe');
    Route::post('/category_match','Cook\CookController@selectCategory');
    Route::get('/recipes/edit/{id}','Cook\CookController@editRecipes');
    Route::patch('/updateRecipe/{id}','Cook\CookController@updateRecipe');
});